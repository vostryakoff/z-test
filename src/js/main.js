import range from "./modules/range";
import tabs from "./modules/tabs";
(function($){
    $(document).ready(() => {
        tabs.init();
        range.init();
    });
})(jQuery);
