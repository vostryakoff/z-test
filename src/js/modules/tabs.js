export default {
    init: function() {
	    let $tab = $('.js-tabs');
	    console.log(1);
	    if ($tab.length) {
	        $tab.each((i, el) => {
	            let _this = $(el);
	            _this.find('.tabs-link').click(function(e) {
	                e.preventDefault();
	                _this.find('.tabs-link').removeClass('active');
	                $(this).addClass('active');
	                _this.find('.tabs-block').removeClass('visible');
	                _this.find('.tabs-block[data-tabs="'+ $(this).data('tabs') +'"]').addClass('visible');
	            });
	        });
	    }
	}
}