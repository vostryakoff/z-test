var webpack = require('webpack');

module.exports = {
    mode: 'development',
    output: {
        filename: 'main.js',
    },
    module: {
        rules: [{
            test: /\.(js|jsx)$/,
            exclude: /(node_modules)/,
            loader: 'babel-loader',
            query: {
                presets: [
                    ['latest', {
                        modules: false
                    }],
                ],
            },
        }, ],
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            slick: 'slick-carousel'
        })
    ]
};